﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace qlykhachsan1
{
    public partial class Form1 : Form
    {
        SqlConnection con = new SqlConnection();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string connectionstring = "Data Source=DESKTOP-QQP2MDV;Initial Catalog=khachsan;Integrated Security=True";
            con.ConnectionString = connectionstring;
            con.Open();
            string sql = "select * from qlkhachsan";
            SqlDataAdapter adp = new SqlDataAdapter(sql,con);
            DataTable tableqlkhachsan = new DataTable();
            adp.Fill(tableqlkhachsan);
            dataGridView1.DataSource = tableqlkhachsan;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtmaphong.Text = dataGridView1.CurrentRow.Cells["maphong"].Value.ToString();
            txttenphong.Text = dataGridView1.CurrentRow.Cells["tenphong"].Value.ToString();
            txtdongia.Text = dataGridView1.CurrentRow.Cells["dongia"].Value.ToString();
            txtmaphong.Enabled = false;
        }
        private void loaddatagridview()
        {
            string sql = "select * from qlkhachsan";
            SqlDataAdapter adp = new SqlDataAdapter(sql, con);
            DataTable tableqlkhachsan = new DataTable();
            adp.Fill(tableqlkhachsan);
            dataGridView1.DataSource = tableqlkhachsan;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            con.Close();
            this.Close();
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            txtmaphong.Text = "";
            txttenphong.Text = "";
            txtdongia.Text = "";
            txtmaphong.Enabled = true;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            string sql = "delete from qlkhachsan where maphong = '" + txtmaphong.Text + "'";
            SqlCommand cmd = new SqlCommand(sql,con);
            cmd.ExecuteNonQuery();
            loaddatagridview();
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            if (txtmaphong.Text == "")
            {
                MessageBox.Show("chua nhap ma phong");
                txtmaphong.Focus();
                return;
            }
            if (txttenphong.Text == "")
            {
                MessageBox.Show("chua nhap ten phong");
                txttenphong.Focus();
                return;
            }


           
            else
            {
                string sql = "insert into qlkhachsan values ('" + txtmaphong.Text + "', '" +
                    txttenphong.Text + "'";
                if (txtdongia.Text != "")
                {
                    sql = sql + "," + txtdongia.Text.Trim();
                }
                sql = sql + ")";
               
                try
                {
                    MessageBox.Show(sql);
                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.ExecuteNonQuery();
                    loaddatagridview();
                }catch(Exception d)
                {
                    MessageBox.Show(d.ToString());
                }
                finally
                {
                    
                }
            }
        }

        private void btnhuy_Click(object sender, EventArgs e)
        {
            btnthem.Enabled = false;
            txtmaphong.Text = "";
            txttenphong.Text = "";
            txtdongia.Text = "";
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            if (txttenphong.Text == "")
            {
                MessageBox.Show("chua nhap ten phong");
                txttenphong.Focus();
                return;
            }
            if (txtdongia.Text == "")
            {
                MessageBox.Show("chua nhap don gia");
                txtdongia.Focus();
                return;
            }
            else
            {
                
                string sql = "update qlkhachsan set tenphong=@tenphong, dongia=@dongia where maphong =@maphong";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("maphong", txtmaphong.Text);
                cmd.Parameters.AddWithValue("tenphong", txttenphong.Text);
                cmd.Parameters.AddWithValue("dongia", txtdongia.Text);
                cmd.ExecuteNonQuery();
                loaddatagridview();
            }
        }

        private void txtdongia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar >= '0') && (e.KeyChar <= '9')) || (Convert.ToInt32(e.KeyChar) == 8) || (Convert.ToInt32(e.KeyChar) == 13))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
       
       
    }
}
