create database khachsan
on
(
name = khachsan_data,
filename ='C:\qly\khachsandata.mdf',
size = 2mb,
filegrowth = 10 mb
)
log on
(
name = khachsan_log,
filename = 'C:\qly\khachsanlog.ldf',
size = 2 mb,
maxsize = unlimated
)
create table qlkhachsan
(
maphong nvarchar(10) primary key,
tenphong nvarchar(50),
dongia int
)
insert into qlkhachsan values ('p101','101a',2000000),
('p1012','102a',2000000),
('p1013','103a',2000000),
('p101a','1013a',5000000)